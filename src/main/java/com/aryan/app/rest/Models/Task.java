package com.aryan.app.rest.Models;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString


@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long taskId;
    @Column
    private String taskName;
    @Column
    private Date deadline;
}
