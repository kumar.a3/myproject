package com.aryan.app.rest.Services;

import com.aryan.app.rest.Models.Task;
import com.aryan.app.rest.Repo.TaskRepo;
import com.aryan.app.rest.dto.Converter;
import com.aryan.app.rest.dto.TaskRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskServices {

    @Autowired
    private TaskRepo taskRepo;

    public Task get_task(Long id) {
        Task task_to_get = taskRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        return task_to_get;
    }
    public void add_task(TaskRequestDto taskRequestDto) {
        Converter converter = new Converter();
        taskRepo.save(converter.dtoToModel(taskRequestDto));
    }

    @Transactional
    public void update_task(Long id,TaskRequestDto taskRequestDto) {
        Task task_to_update = taskRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        task_to_update.setTaskName(taskRequestDto.getTaskName());
        task_to_update.setDeadline(taskRequestDto.getDeadline());
    }

    public void delete_task(Long id)
    {
        Task task_to_delete = taskRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        taskRepo.delete(task_to_delete);
    }

}
