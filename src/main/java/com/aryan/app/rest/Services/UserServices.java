package com.aryan.app.rest.Services;

import com.aryan.app.rest.Models.User;
import com.aryan.app.rest.Repo.UserRepo;
import com.aryan.app.rest.dto.UserRequestDto;
import com.aryan.app.rest.dto.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class UserServices {

    @Autowired
    private UserRepo userRepo;

    public User get_user(Long id) {
        User user_to_get = userRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        return user_to_get;
    }
    public void add_user(UserRequestDto userRequestDto) {
        Converter converter = new Converter();
        userRepo.save(converter.dtoToModel(userRequestDto));
    }

    @Transactional
    public void update_user(Long id,UserRequestDto userRequestDto) {
        User user_to_update = userRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        user_to_update.setFirstName(userRequestDto.getFirstName());
        user_to_update.setLastName(userRequestDto.getLastName());
        user_to_update.setAge(userRequestDto.getAge());
        user_to_update.setOccupation(userRequestDto.getOccupation());
    }

    public void delete_user(Long id)
    {
        User user_to_delete = userRepo.findById(id).orElseThrow(
                () -> new IllegalStateException("User with ID" + id + "is not present"));
        userRepo.delete(user_to_delete);
    }

}
