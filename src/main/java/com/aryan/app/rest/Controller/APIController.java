package com.aryan.app.rest.Controller;

import com.aryan.app.rest.Models.User;
import com.aryan.app.rest.Repo.UserRepo;
import com.aryan.app.rest.Services.UserServices;
import com.aryan.app.rest.dto.UserRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class APIController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserServices userService;
    @Autowired(required = false)
    private User user;
    @GetMapping("/")
    public String getPage()
    {
        return "Welcome..";
    }

    @GetMapping("/user")
    public List<User> getUser()
    {
        return userRepo.findAll();
    }

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable Long id)
    {
        return userService.get_user(id);
    }

    @PostMapping("/user")
    public List<User> addUser(@RequestBody UserRequestDto userRequestDto)
    {
        userService.add_user(userRequestDto);
        return userRepo.findAll();
    }

    @PutMapping("/user/{id}")
    public List<User> updateUser(@PathVariable Long id,@RequestBody UserRequestDto userRequestDto) {
        userService.update_user(id,userRequestDto);
        return userRepo.findAll();
    }

    @DeleteMapping("/user/{id}")
    public List<User> deleteUser(@PathVariable Long id)
    {
        userService.delete_user(id);
        return userRepo.findAll();
    }


}
