package com.aryan.app.rest.Controller;


import com.aryan.app.rest.Models.Task;
import com.aryan.app.rest.Repo.TaskRepo;
import com.aryan.app.rest.Services.TaskServices;
import com.aryan.app.rest.dto.TaskRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {

    @Autowired
    private TaskRepo taskRepo;

    @Autowired
    private TaskServices taskService;
    @Autowired(required = false)
    private Task task;

    @GetMapping("/task")
    public List<Task> getTask()
    {
        return taskRepo.findAll();
    }

    @GetMapping("/task/{id}")
    public Task getTask(@PathVariable Long id)
    {
        return taskService.get_task(id);
    }

    @PostMapping("/task")
    public List<Task> addTask(@RequestBody TaskRequestDto taskRequestDto)
    {
        taskService.add_task(taskRequestDto);
        return taskRepo.findAll();
    }

    @PutMapping("/task/{id}")
    public List<Task> updateTask(@PathVariable Long id,@RequestBody TaskRequestDto taskRequestDto) {
        taskService.update_task(id,taskRequestDto);
        return taskRepo.findAll();
    }

    @DeleteMapping("/task/{id}")
    public List<Task> deleteTask(@PathVariable Long id)
    {
        taskService.delete_task(id);
        return taskRepo.findAll();
    }
}
