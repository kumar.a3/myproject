package com.aryan.app.rest.dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserResponseDto {
    private long id;
    private String firstName;
    private  String lastName;
    private int age;
    private String occupation;
}
