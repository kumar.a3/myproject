package com.aryan.app.rest.dto;
import  com.aryan.app.rest.Models.User;
import com.aryan.app.rest.Models.Task;
import org.springframework.stereotype.Component;

import lombok.*;

import jakarta.persistence.Column;

@Component
public class Converter {
    public User dtoToModel(UserRequestDto userRequestDto)
    {
        User model  = new User();
        model.setFirstName(userRequestDto.getFirstName());
        model.setLastName(userRequestDto.getLastName());
        model.setAge(userRequestDto.getAge());
        model.setOccupation(userRequestDto.getOccupation());
        return model;
    }

    public Task dtoToModel(TaskRequestDto taskRequestDto)
    {
        Task model  = new Task();
        model.setTaskName(taskRequestDto.getTaskName());
        model.setDeadline(taskRequestDto.getDeadline());
        return model;
    }

    public UserResponseDto modelToResponse(User user)
    {
        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setFirstName(user.getFirstName());
        userResponseDto.setLastName(user.getLastName());
        return userResponseDto;
    }

}
