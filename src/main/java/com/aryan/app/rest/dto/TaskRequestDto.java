package com.aryan.app.rest.dto;



import jakarta.persistence.Column;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class TaskRequestDto {
    private String taskName;
    private Date deadline;
}
