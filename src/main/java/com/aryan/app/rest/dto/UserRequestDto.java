package com.aryan.app.rest.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class UserRequestDto {
    private String firstName;
    private String lastName;
    private int age;
    private String occupation;



}
