package com.aryan.app.rest.Repo;

import com.aryan.app.rest.Models.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepo extends JpaRepository<Task, Long> {
}
